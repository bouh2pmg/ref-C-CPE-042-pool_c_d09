// No Header
// 04/07/2017 - 13:56:16

#include "rubiks.h"

void	build_last_line(int **tab)
{
  int	s_lines[4];
  int	v_lines[4];
  int	cols[4];
  int	*ret_s;
  int	*ret_v;
  int	tmp;
  
  s_lines[0] = BLOCKED;
  s_lines[1] = BLOCKED;
  s_lines[2] = BLOCKED;
  s_lines[3] = EMPTY;

  v_lines[0] = EMPTY;
  v_lines[1] = EMPTY;
  v_lines[2] = EMPTY;
  v_lines[3] = BLOCKED;

  cols[0] = EMPTY;
  cols[0] = EMPTY;
  cols[0] = EMPTY;
  cols[0] = EMPTY;

  tmp = tab[3][3];
  while ((ret_s = look_for_space(tab, s_lines, cols, tmp)) != NULL)
    {
      while (ret_s[0] < 2)
	{
	  algo_column_reverse(tab, ret_s[1]);
	  ret_s[0]++;
	}
      while (ret_s[1] < 2)
	{
	  algo_line_reverse(tab, ret_s[0]);
	  ret_s[1]++;
	}
      ret_v = look_for_value(tab, v_lines, cols, tmp);
      while (ret_v[1] < 2)
	{
	  algo_line_reverse(tab, ret_v[0]);
	  ret_v[1]++;
	}
      while (ret_v[0] < 2)
	{
	  algo_column_reverse(tab, ret_v[1]);
	  ret_v[0]++;
	}
      rotate_lines(tab, ret_v[0], ret_v[1] - ret_s[1]);
      rotate_columns(tab, ret_s[1], ret_s[0] - ret_v[0]);
    }
}

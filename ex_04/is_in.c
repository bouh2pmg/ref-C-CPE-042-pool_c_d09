// No Header
// 25/04/2017 - 11:40:17

#include "rubiks.h"

int	is_in_line(int **tab, int line, int value)
{
  int	i;

  i = 0;
  while (i != 4)
    {
      if (tab[line][i] == value)
	return (0);
      i++;
    }
  return (1);
}

int	is_in_col(int **tab, int col, int value)
{
  int	i;

  i = 0;
  while (i != 4)
    {
      if (tab[i][col] == value)
	return (0);
      i++;
    }
  return (1);
}

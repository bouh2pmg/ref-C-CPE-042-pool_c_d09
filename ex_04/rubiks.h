// No Header
// 03/05/2017 - 16:32:02

#include <stdlib.h>
#include <stdio.h>
//#define MAGIC_SQR_DBG__ 0xADe4Db33
#define EMPTY 0
#define BLOCKED 1
void	*xmalloc(size_t s);
  
/*
** EXO #1
** EASY
*/
void	print_tab(int **tab);
int	check_square(int **tab);

/*
** EXO #2
** EASY
*/
void	algo_line(int **tab, int l);
void	algo_column(int **tab, int c);
void	algo_square(int **tab, int s);
void	get_square_coord(int s, int *l, int *c);

/*
** EXO #3
** VERY EASY
*/
void	algo_line_reverse(int **tab, int l);
void	algo_column_reverse(int **tab, int c);
void	algo_square_reverse(int **tab, int s);

/*
** EXO #4
** VERY EASY
*/
int	is_in_line(int **tab, int line, int value);
int	is_in_col(int **tab, int col, int value);

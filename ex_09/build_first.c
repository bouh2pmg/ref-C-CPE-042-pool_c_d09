// No Header
// 25/04/2017 - 13:16:53

#include "rubiks.h"

void	build_first_line(int **tab)
{
  int	s_lines[4];
  int	v_lines[4];
  int	cols[4];
  int	*ret_s;
  int	*ret_v;
  int	tmp;
  
  s_lines[0] = EMPTY;
  s_lines[1] = BLOCKED;
  s_lines[2] = BLOCKED;
  s_lines[3] = BLOCKED;

  v_lines[0] = BLOCKED;
  v_lines[1] = EMPTY;
  v_lines[2] = EMPTY;
  v_lines[3] = EMPTY;

  cols[0] = EMPTY;
  cols[0] = EMPTY;
  cols[0] = EMPTY;
  cols[0] = EMPTY;

  tmp = tab[0][0];
  while ((ret_s = look_for_space(tab, s_lines, cols, tmp)) != NULL)
    {
      ret_v = look_for_value(tab, v_lines, cols, tmp);
      rotate_lines(tab, ret_v[0], ret_s[1] - ret_v[1]);
      rotate_columns(tab, ret_s[1], ret_s[0] - ret_v[0]);
    }
}

void	line_to_square(int **tab, int line)
{
  algo_square(tab, line);
  algo_square(tab, line);
  algo_line(tab, line);
  algo_line(tab, line);
}

// No Header
// 25/04/2017 - 11:37:36

#include "rubiks.h"

int	*look_for_value(int **tab, int *lines, int *cols, int value)
{
  int	l;
  int	c;
  
  l = 0;
  while (l != 4)
    {
      if (lines[l] == BLOCKED)
	{
	  l++;
	  continue;
	}
      c = 0;
      while (c != 4)
	{
	  if (cols[c] == BLOCKED)
	    {
	      c++;
	      continue;
	    }
	  if (tab[l][c] == value)
	    return (found_range(l, c));
	  c++;
	}
      l++;
    }
  return (NULL);
}

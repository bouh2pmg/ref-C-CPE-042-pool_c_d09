// No Header
// 27/04/2017 - 01:06:42

#include "rubiks.h"

//#ifdef MAGIC_SQR_DBG__
void	magic(int **tab, char *str, int l)
{
  /* if (MAGIC_SQR_DBG__ == 0xADe4Db33) */
  /*   { */
      system("sleep 1");
      system("clear");
      printf(str, l);
      print_tab(tab);
    /* } */
}
//#endif
void	algo_line_reverse(int **tab, int l)
{
  int	c;
  int	tmp;

  tmp = tab[l][3];
  c = 3;
  while (c != 0)
    {
      tab[l][c] = tab[l][c - 1];
      c--;
    }
  tab[l][0] = tmp;
#ifdef MAGIC_SQR_DBG__
  magic(tab, "Rotate Right line %d\n", l);
#endif
}

void	algo_column_reverse(int **tab, int c)
{
  int	l;
  int	tmp;

  tmp = tab[3][c];
  l = 3;
  while (l != 0)
    {
      tab[l][c] = tab[l - 1][c];
      l--;
    }
  tab[0][c] = tmp;
#ifdef MAGIC_SQR_DBG__
  magic(tab, "Rotate Down column %d\n", c);
#endif
}

void	algo_square_reverse(int **tab, int s)
{
  int	l;
  int	c;
  int	tmp;
  int	i;

  get_square_coord(s, &l, &c);
  tmp = tab[l][c];
  tab[l][c] = tab[l][c + 1];
  tab[l][c + 1] = tab[l + 1][c + 1];
  tab[l + 1][c + 1] = tab[l + 1][c];
  tab[l + 1][c] = tmp;
#ifdef MAGIC_SQR_DBG__
  magic(tab, "Rotate Counter Clockwise square %d\n", s);
#endif
}

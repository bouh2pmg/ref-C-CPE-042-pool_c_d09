// No Header
// 25/04/2017 - 11:41:33

#include "rubiks.h"

int	*found_range(int l, int c)
{
  int	*r;

  r = xmalloc(sizeof(int) * 2);
  r[0] = l;
  r[1] = c;
  return (r);
}

int	*look_for_space(int **tab, int *lines, int *cols, int value)
{
  int	l;
  int	c;
  
  l = 0;
  while (l != 4)
    {
      if (lines[l] == BLOCKED)
	{
	  l++;
	  continue;
	}
      c = 0;
      while (c != 4)
	{
	  if (cols[c] == BLOCKED)
	    {
	      c++;
	      continue;
	    }
	  if (tab[l][c] != value)
	    return (found_range(l, c));
	  c++;
	}
      l++;
    }
  return (NULL);
}

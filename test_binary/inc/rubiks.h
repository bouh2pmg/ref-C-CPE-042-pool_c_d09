// No Header
// 10/10/2017 - 12:08:08

#include <stdlib.h>
#include <stdio.h>
#define MAGIC_SQR_DBG__ 0xADe4Db33
#define EMPTY 0
#define BLOCKED 1
void	*xmalloc(size_t s);

#ifdef MAGIC_SQR_DBG__
void	magic(int **tab, char *str, int l);
#endif

/*
** EXO #1
** EASY
*/
void	print_tab(int **tab);
int	check_square(int **tab);

/*
** EXO #2
** EASY
*/
void	algo_line(int **tab, int l);
void	algo_column(int **tab, int c);
void	algo_square(int **tab, int s);
void	get_square_coord(int s, int *l, int *c);

/*
** EXO #3
** VERY EASY
*/
void	algo_line_reverse(int **tab, int l);
void	algo_column_reverse(int **tab, int c);
void	algo_square_reverse(int **tab, int s);

/*
** EXO #4
** VERY EASY
*/
int	is_in_line(int **tab, int line, int value);
int	is_in_col(int **tab, int col, int value);

/*
** EXO #5
** EASY
*/
int	*found_range(int l, int c);
int	*look_for_space(int **tab, int *lines, int *cols, int value);

/*
** EXO #6
** VERY EASY
*/
int	*look_for_value(int **tab, int *lines, int *cols, int value);

/*
** EXO #7
**
*/
void	rotate_lines(int **tab, int line, int offset);
void	rotate_columns(int **tab, int col, int offset);

/*
** EXO #8
** NOT SO HARD
*/
void	build_first_line(int **tab);
void	line_to_square(int **tab, int line);

/*
** EXO #9
** VERY VERY HARD
*/
void	build_last_line(int **tab);
void	build_final_line(int **tab);

/*
** EXO #10
** 
*/
void	finish_it(int **tab);

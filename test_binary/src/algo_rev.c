// No Header
// 10/10/2017 - 12:08:39

#include "rubiks.h"

void	magic(int **tab, char *str, int l)
{
  system("sleep 1");
  system("clear");
  printf(str, l);
  print_tab(tab);
}

void	algo_line_reverse(int **tab, int l)
{
  int	c;
  int	tmp;

  tmp = tab[l][3];
  c = 3;
  while (c != 0)
    {
      tab[l][c] = tab[l][c - 1];
      c--;
    }
  tab[l][0] = tmp;
#ifdef MAGIC_SQR_DBG__
  magic(tab, "Rotate Right line %d\n", l);
#endif
}

void	algo_column_reverse(int **tab, int c)
{
  int	l;
  int	tmp;

  tmp = tab[3][c];
  l = 3;
  while (l != 0)
    {
      tab[l][c] = tab[l - 1][c];
      l--;
    }
  tab[0][c] = tmp;
#ifdef MAGIC_SQR_DBG__
  magic(tab, "Rotate Down column %d\n", c);
#endif
}

void	algo_square_reverse(int **tab, int s)
{
  int	l;
  int	c;
  int	tmp;

  get_square_coord(s, &l, &c);
  tmp = tab[l][c];
  tab[l][c] = tab[l][c + 1];
  tab[l][c + 1] = tab[l + 1][c + 1];
  tab[l + 1][c + 1] = tab[l + 1][c];
  tab[l + 1][c] = tmp;
#ifdef MAGIC_SQR_DBG__
  magic(tab, "Rotate Counter Clockwise square %d\n", s);
#endif
}

// No Header
// 10/10/2017 - 12:08:13

#include "rubiks.h"
#include <time.h>

void	my_swap(int *a, int *b)
{
  int	tmp;

  tmp = *a;
  *a = *b;
  *b = tmp;
}

void	shuffle_the_square(int **tab, int loops)
{
  int	rdm[4];

  while (loops >= 0)
    {
      rdm[0] = random() % 4;
      rdm[1] = random() % 4;
      rdm[2] = random() % 4;
      rdm[3] = random() % 4;
      my_swap(&tab[rdm[0]][rdm[2]], &tab[rdm[1]][rdm[3]]);
      loops--;
    }
}

void	*xmalloc(size_t s)
{
  void	*data;
  
  if ((data = malloc(s)) == NULL)
    exit(-127);
  return (data);
}

void	verif_return(int *ret)
{
  if (ret != NULL )
    printf("line: \t\t%d\nColumn: \t%d\n", ret[0], ret[1]);
  else
    printf("Nothing found in the given range.\n");
}

void	build_first_square(int **tab)
{
  build_first_line(tab);
  line_to_square(tab, 0);
}

void	build_second_square(int **tab)
{
  build_last_line(tab);
  line_to_square(tab, 3);
  algo_column(tab, 2);
  algo_column(tab, 3);
  algo_column(tab, 2);
  algo_column(tab, 3);
}

void	finish_it(int **tab)
{
  build_final_line(tab);
}

int main()
{
  int	**tab = malloc(4 * sizeof(int*));
  tab[0] = malloc(4 * sizeof(int));
  tab[0][0] = 0;
  tab[0][1] = 0;
  tab[0][2] = 1;
  tab[0][3] = 1;
  tab[1] = malloc(4 * sizeof(int));
  tab[1][0] = 0;
  tab[1][1] = 0;
  tab[1][2] = 1;
  tab[1][3] = 1;
  tab[2] = malloc(4 * sizeof(int));
  tab[2][0] = 2;
  tab[2][1] = 2;
  tab[2][2] = 3;
  tab[2][3] = 3;
  tab[3] = malloc(4 * sizeof(int));
  tab[3][0] = 2;
  tab[3][1] = 2;
  tab[3][2] = 3;
  tab[3][3] = 3;

  /* srandom(1491986737); */
  /* srandom(1493122164); */
  int	seed = time(NULL);
  srandom(seed);

  //  printf("Check Square : %d\n", check_square(tab));
  magic(tab, "Used seed: %d\n", seed);
  shuffle_the_square(tab, 150);

  magic(tab, "Shuffled\n", 0);

  build_first_square(tab);
  build_second_square(tab);
  finish_it(tab);
  if (check_square(tab) == 0)
    magic(tab, "Done\n", 0);
  return (0);
}

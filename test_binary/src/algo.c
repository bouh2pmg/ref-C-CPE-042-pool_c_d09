// No Header
// 10/10/2017 - 12:08:30

#include "rubiks.h"

void	algo_line(int **tab, int l)
{
  int	c;
  int	tmp;

  tmp = tab[l][0];
  c = 0;
  while (c != 3)
    {
      tab[l][c] = tab[l][c + 1];
      c++;
    }
  tab[l][3] = tmp;
#ifdef MAGIC_SQR_DBG__
  magic(tab, "Rotate Left line %d\n", l);
#endif
}

void	algo_column(int **tab, int c)
{
  int	l;
  int	tmp;

  tmp = tab[0][c];
  l = 0;
  while (l != 3)
    {
      tab[l][c] = tab[l + 1][c];
      l++;
    }
  tab[3][c] = tmp;
#ifdef MAGIC_SQR_DBG__
  magic(tab, "Rotate Top column %d\n", c);
#endif
}

void	get_square_coord(int s, int *l, int *c)
{
  if (s < 2)
    *l = 0;
  else
    *l = 2;
  if (s % 2 == 0)
    *c = 0;
  else
    *c = 2;  
}

void	algo_square(int **tab, int s)
{
  int	l;
  int	c;
  int	tmp;

  get_square_coord(s, &l, &c);
  tmp = tab[l][c];
  tab[l][c] = tab[l + 1][c];
  tab[l + 1][c] = tab[l + 1][c + 1];
  tab[l + 1][c + 1] = tab[l][c + 1];
  tab[l][c + 1] = tmp;
#ifdef MAGIC_SQR_DBG__
  magic(tab, "Rotate Clockwise square %d\n", s);
#endif
}

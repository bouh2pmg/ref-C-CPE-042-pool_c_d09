// No Header
// 25/04/2017 - 12:14:39

#include "rubiks.h"

void	rotate_lines_rev(int **tab, int line, int offset)
{
  while (offset > 0)
    {
      algo_line_reverse(tab, line);
      offset--;
    }  
}

void	rotate_lines(int **tab, int line, int offset)
{
  if (offset > 0)
    {
      rotate_lines_rev(tab, line, offset);
      return ;
    }
  offset = -offset;
  while (offset > 0)
    {
      algo_line(tab, line);
      offset--;
    }
}

void	rotate_columns_rev(int **tab, int col, int offset)
{
  while (offset > 0)
    {
      algo_column_reverse(tab, col);
      offset--;
    }  
}

void	rotate_columns(int **tab, int col, int offset)
{
  if (offset > 0)
    {
      rotate_columns_rev(tab, col, offset);
      return ;
    }
  offset = -offset;
  while (offset > 0)
    {
      algo_column(tab, col);
      offset--;
    }
}

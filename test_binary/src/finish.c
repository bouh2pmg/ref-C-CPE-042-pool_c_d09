// No Header
// 10/10/2017 - 12:07:03

#include "rubiks.h"

int	follow(int **tab, int value)
{
  int	i;

  i = 0;
  while (i < 3)
    {
      if (tab[2][i] == tab[2][i + 1] && tab[2][i] == value)
	{
	  while (i > 0)
	    {
	      algo_line(tab, 2);
	      i--;
	    }
	  return (0);
	}
      i++;
    }
  return (1);
}

void	build_final_line(int **tab)
{
  int	tmp;

  while (tab[2][0] != tab[2][1])
    {
      algo_line(tab, 2);
      algo_square(tab, 3);
    }
  tmp = tab[2][0];
  algo_square(tab, 2);
  algo_square(tab, 2);
  while (follow(tab, tmp) != 0)
    {
      if (tab[2][3] != tab[3][3])
	algo_line(tab, 2);
      algo_square(tab, 3);
    }
}

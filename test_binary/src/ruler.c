// No Header
// 10/10/2017 - 11:59:09

#include "rubiks.h"

/*
** EASY
*/

void	print_tab(int **tab)
{
  int	l;

  l = 0;
  printf("-----------------\n");
  while (l != 4)
    {
      printf("| \e[1;3%dm%d\e[0m | \e[1;3%dm%d\e[0m | \e[1;3%dm%d\e[0m | \e[1;3%dm%d\e[0m |\n",
	     tab[l][0] * 2,tab[l][0],
	     tab[l][1] * 2,tab[l][1],
	     tab[l][2] * 2,tab[l][2],
	     tab[l][3] * 2,tab[l][3]);
      printf("-----------------\n");
      l++;
    }
  printf("\n");
}

int	check_inner_square(int **tab, int c, int l)
{
  int	r;
  int	tmp;
  int	i;
  int	j;
  
  r = 0;
  j = 0;
  tmp = tab[l][c];
  while (j != 2)
    {
      i = 0;
      while (i != 2)
	{
	  if (tmp != tab[l + j][c + i])
	    r++;
	  i++;
	}
      j++;
    }
  return (r);
}

int	check_square(int **tab)
{
  int	ret;

  ret = check_inner_square(tab, 0, 0);
  ret += check_inner_square(tab, 2, 0);
  ret += check_inner_square(tab, 0, 2);
  ret += check_inner_square(tab, 2, 2);
  if (ret > 0)
    return (1);
  return (ret);
}
